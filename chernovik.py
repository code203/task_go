class Task:
    def __init__(self, content: str, is_complete: bool = False):
        self.content = content
        self.is_complete = is_complete

    def change_status(self):
        self.is_complete = not self.is_complete

    def __str__(self):
        return f' {self.content}. Статус: {["не ", ""][self.is_complete]}выполнена'


class TaskList:

    def __init__(self):
        self.tasks = []

    def add_task(self, task: Task):
        self.tasks.append(task)
        print('Задача добавлена.')

    def show_all(self):
        for i, task in enumerate(self.tasks):
            print(f'{i + 1}. {task}')

    def remove_task(self, index:int):
        self.tasks.pop(index)
        print(f'Задача {index} удалена.')

    def change_task_status(self, index: int):

        self.tasks[index].change_status()


class Interface:
    def __init__(self, static_day='1.Список заданий\n', enter='2.Записать новое задание\n', pop='3.Удалить зазачу\n',
                 change='4.Выполнить задачу\n',
                 exit='5.Выход\n', ):
        self.static_day = static_day
        self.enter = enter
        self.exit = exit
        self.pop = pop
        self.change = change

    def choice(self):

        global a
        print('-----------------\n' + self.static_day, self.enter, self.pop, self.change,
              self.exit + '-----------------')
        my_tasks = TaskList()


        while True:

            a = input('Введите значение:')

            if a.isdigit() == False:
                print('Введено не число')
                continue
            a = int(a)
            if a == 1:
                my_tasks.show_all()
            elif a == 2:
                my_tasks.add_task(task=Task(input('Введите новое задание')))
            elif a == 3:
                (my_tasks.remove_task(index=int(input('Введите какую задачу хотите удалить'))))

            elif a == 4:

                my_tasks.change_task_status(index=int(input('Статус какой задачи хотите сменить? ')))
            elif a == 5:
                print('Программа завершила работу')
                exit()
            else:
                print('Вы ввели не корректное значение')


Interface().choice()